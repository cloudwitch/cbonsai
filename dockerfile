FROM ubuntu:rolling

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get -y update &&\
    apt-get -y install libncursesw5-dev \
    build-essential \
    pkg-config \
    scdoc

COPY cbonsai.c cbonsai.c
COPY cbonsai.scd cbonsai.scd
COPY Makefile Makefile

RUN make -j $(nproc) install

ENTRYPOINT [ "cbonsai" ]

CMD [ "--life 60" "--multiplier 5" "--live" "--infinite" "-t 0.01" "-b 1" ]
